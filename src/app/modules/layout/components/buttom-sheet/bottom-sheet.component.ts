import {Component, Inject, Input} from '@angular/core';

@Component({
    selector: 'bottom-sheet',
    templateUrl: './bottom-sheet.component.html'
})

export class BottomSheetComponent {
    constructor(
        private BottomSheetRef: MatBottomSheetRef<BottomSheetComponent>,
        @Inject(MAT_BOTTOM_SHEET_DATA) public options: any
    ) {}

    public openLink(event: MouseEvent, clickCallback): void {
        this.BottomSheetRef.dismiss();
        event.preventDefault();

        if (clickCallback) {
            clickCallback(event);
        }
    }
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetModule, MatBottomSheetRef, MatListModule} from '@angular/material';

@NgModule({
    imports: [CommonModule, MatBottomSheetModule, MatListModule],
    declarations: [BottomSheetComponent],
    exports: [BottomSheetComponent],
    entryComponents: [BottomSheetComponent]
})

export class BottomSheetModule {}

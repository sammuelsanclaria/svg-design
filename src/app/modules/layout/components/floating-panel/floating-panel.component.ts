import {Component, Input} from '@angular/core';

@Component({
    selector: 'floating-panel',
    templateUrl: './floating-panel.component.html'
})

export class FloatingPanelComponent {
    @Input() buttons: any = {};

    constructor() {}
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material';

@NgModule({
    imports: [CommonModule, MatButtonModule],
    declarations: [FloatingPanelComponent],
    exports: [FloatingPanelComponent]
})

export class FloatingPanelModule {}

import { Injectable } from '@angular/core';
import {ImageService} from "./image.service";

@Injectable({
    providedIn: 'root'
})

export class CanvasImageService {
    constructor(private imageService: ImageService) {}

    public getAppropriateSizedImage(imageWidth, sizedImages) {
        let appropriateImageWidth = null;
        let largestImageWidth = 0;

        for (const width in sizedImages) {
            if (sizedImages.hasOwnProperty(width)) {
                if (width >= imageWidth
                    && (!appropriateImageWidth || width < appropriateImageWidth)) {
                    appropriateImageWidth = parseInt(width);
                }

                if (parseInt(width) > largestImageWidth) {
                    largestImageWidth = parseInt(width);
                }
            }
        }

        if (sizedImages) {
            if (sizedImages[appropriateImageWidth]) {
                return sizedImages[appropriateImageWidth];
            } else {
                return sizedImages[largestImageWidth];
            }
        }

        return null;
    }
}

import { Injectable } from '@angular/core';
import {ReplaySubject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class DragHandlerService {
    constructor() {}

    public bindDragEventsToElement(containerElement, element) {
        const event = new ReplaySubject();

        let initialX = 0;
        let initialY = 0;
        let active = false;

        const onDragStartHandler = (e) => {
            if (e.target === element) {
                const dragStartResponse = this.elementDragStart(e, element);
                initialX = dragStartResponse.initialX;
                initialY = dragStartResponse.initialY;
                active = dragStartResponse.active;

                event.next({
                    event_code: 'DRAG_START',
                    event: e
                });
            }
        };

        const onDragHandler = (e) => {
            if (active) {
                const displacement = this.elementDrag(e, initialX, initialY, active);
                if (displacement) {
                    initialX = displacement.currentX;
                    initialY = displacement.currentY;

                    if (!(displacement.xDisplacement === 0
                        && displacement.yDisplacement === 0)) {

                        event.next({
                            event_code: 'DRAGGING',
                            event: e,
                            displacement: displacement
                        });
                    }
                }
            }
        };

        const onDragEndHandler = (e) => {
            if (active) {
                active = false;
                event.next({
                    event_code: 'DRAG_END',
                    event: e
                });
            }
        };

        // on drag start events
        containerElement.addEventListener('mousedown', (e) => { onDragStartHandler(e); }, false);
        containerElement.addEventListener('touchstart', (e) => { onDragStartHandler(e); }, false);

        // on drag events
        containerElement.addEventListener('mousemove', (e) => { onDragHandler(e); }, false);
        containerElement.addEventListener('touchmove', (e) => { onDragHandler(e); }, false);

        // on drag end events
        containerElement.addEventListener('mouseup', (e) => { onDragEndHandler(e); }, false);
        containerElement.addEventListener('touchend', (e) => { onDragEndHandler(e); }, false);

        return event.asObservable();
    }


    public elementDragStart(e, element) {
        e.stopPropagation();
        e.preventDefault();
        return {
            initialX: e.type === 'touchstart' ? e.touches[0].clientX : e.clientX,
            initialY: e.type === 'touchstart' ? e.touches[0].clientY : e.clientY,
            active: e.target === element
        };
    }


    public elementDrag(e, elementInitialX, elementInitialY, elementActive) {
        e.stopPropagation();
        e.preventDefault();
        if (elementActive) {
            let currentX = 0;
            let currentY = 0;

            if (e.type === 'touchmove') {
                currentX = e.touches[0].clientX;
                currentY = e.touches[0].clientY;
            } else {
                currentX = e.clientX;
                currentY = e.clientY;
            }

            const xDisplacement = currentX - elementInitialX;
            const yDisplacement = currentY - elementInitialY;
            const hypotenuse = Math.sqrt(Math.pow(xDisplacement, 2) + Math.pow(yDisplacement, 2));

            return {
                e,
                hypotenuse,
                xDisplacement,
                yDisplacement,
                currentX,
                currentY
            };
        }

        return null;
    }
}

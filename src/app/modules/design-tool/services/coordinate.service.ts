import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class CoordinateService {

    public getCroppingIncrease(element, increase) {
        return (element.clipping.size.width / element.size.width) * increase;
    }

    public getSecondaryHypotenuse(element, increase, dragHandlerCode) {
        switch (dragHandlerCode) {
            case 'L':
                return (element.size.height * increase) / (element.size.width - increase);

            case 'R':
                return (element.size.height * increase) / (element.size.width - increase);

            case 'B':
                return ((element.size.height - increase) * increase) / element.size.width;

            default:
                return (element.size.height * increase) / (element.size.width - increase);
        }
    }

    public getCornerAngle(element) {
        return Math.atan((element.size.height / 2) / (element.size.width / 2)) * (180 / Math.PI);
    }

    public getCursorAngleFromElementOrigin(element, displacement) {
        const centerX = (element.coordinates.x) + (element.size.width / 2);
        const centerY = (element.coordinates.y) + (element.size.height / 2);
        const radians = Math.atan2(displacement.e.pageX - centerX,  displacement.e.pageY - centerY);

        return (radians * (180 / Math.PI) * -1) + 180;
    }

    private calculatePointsDistance(point1, point2) {
        return Math.sqrt(Math.pow((point1.x - point2.x), 2) + Math.pow((point1.y - point2.y), 2));
    }

    private computeAngleUsingSineRule(sideLength, otherSideObject) {
        return Math.asin((sideLength * Math.sin(otherSideObject.rotation * Math.PI / 180)) / otherSideObject.distance) * 180 / Math.PI;
    }

    private getPointIntersection(point1, point2, point1Slope) {
        if (point1Slope === 0) {
            return {
                x: point1.x + point2.x,
                y: 0
            };

        } else if (isNaN(point1Slope)) {
            return {
                x: 0,
                y: point1.y + point2.y
            };
        }

        // solve for y-intercepts
        const pYIntercept1 = point1.y - (point1Slope * point1.x);
        const pYIntercept2 = point2.y + (point2.x / point1Slope);

        // solve for abscissa
        const abscissa = (pYIntercept2 - pYIntercept1) / (point1Slope + (1 / point1Slope));
        const ordinate = point1Slope * abscissa + pYIntercept1;

        return {
            x: abscissa,
            y: ordinate
        };
    }

    public getSideResizeObject(elementRotation, displacement, dragHanlderCode) {
        displacement.yDisplacement = -1 * displacement.yDisplacement;

        elementRotation = -elementRotation;
        switch (dragHanlderCode) {
            case 'L':
                elementRotation -= 180;
                elementRotation = elementRotation % 360;
                break;
            case 'B':
                elementRotation -= 90;
                elementRotation = elementRotation % 360;
                break;
            case 'T':
                elementRotation -= 270;
                elementRotation = elementRotation % 360;
                break;
        }

        const rotationInRadian = elementRotation * Math.PI / 180;
        const slope = Math.tan(rotationInRadian);
        const perpendicularPoint = this.getPointIntersection(
            {x: 0, y: 0},
            {
                x: displacement.xDisplacement,
                y: displacement.yDisplacement,
            },
            slope
        );

        let hypotenuse = Math.sqrt(Math.pow(perpendicularPoint.x, 2) + Math.pow(perpendicularPoint.y, 2));

        // solve for direction
        const resultantRotation = this.getResultantRotation(displacement, elementRotation);

        if (resultantRotation > 90 && resultantRotation < 270) {
            hypotenuse *= -1;
        }

        return hypotenuse;
    }

    public getCenterCompensatorCoordinates(hypotenuse, elementRotation, dragHanlderCode) {
        elementRotation = -elementRotation;
        switch (dragHanlderCode) {
            case 'L':
                elementRotation -= 180;
                elementRotation = elementRotation % 360;
                break;
            case 'T':
                elementRotation -= 180;
                elementRotation = elementRotation % 360;
                break;
        }

        const rotationInRadian = elementRotation * Math.PI / 180;

        return {
            xCompensator : (hypotenuse / 2) - ((hypotenuse / 2) * Math.cos(rotationInRadian)),
            yCompensator : (hypotenuse / 2) * Math.sin(rotationInRadian)
        };
    }

    private getResultantRotation(displacement, elementRotation) {
        let resultantRotation = 0;
        const movementRotation = Math.atan(displacement.yDisplacement / displacement.xDisplacement) * 180 / Math.PI;

        if (displacement.yDisplacement > 0 && displacement.xDisplacement > 0
            || displacement.yDisplacement === 0 && displacement.xDisplacement > 0) {
            // Quadrant 1 or +X
            resultantRotation = movementRotation - elementRotation;

        } else if (displacement.yDisplacement > 0 && displacement.xDisplacement < 0
            || displacement.yDisplacement === 0 && displacement.xDisplacement < 0) {
            // Quadrant 2 or -X
            resultantRotation = 180 + movementRotation - elementRotation;

        } else if (displacement.yDisplacement < 0 && displacement.xDisplacement < 0) {
            // Quadrant 3
            resultantRotation = 180 + movementRotation - elementRotation;

        } else if (displacement.yDisplacement < 0 && displacement.xDisplacement > 0
            || displacement.yDisplacement < 0 && displacement.xDisplacement === 0) {
            // Quadrant 4 or -Y
            resultantRotation = 360 + movementRotation - elementRotation;

        } else if (displacement.yDisplacement > 0 && displacement.xDisplacement === 0) {
            // +Y
            resultantRotation = 90 - elementRotation;
        }

        return resultantRotation >= 360 ? resultantRotation - 360 : resultantRotation;
    }
}

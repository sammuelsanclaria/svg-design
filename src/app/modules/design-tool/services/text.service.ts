import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class TextService {
    public drawText(textData, canvas) {
        const canvasContext = canvas.getContext('2d');

        canvasContext.save();
        canvasContext.font = textData.font_size + 'px Georgia';

        // const abscissa = textData.coordinates.x - canvasContext.measureText(textData.content).width / 2;
        // const ordinate = textData.coordinates.y + textData.font_size / 2;

        const canvasAbscissa = canvas.width / 2 + textData.coordinates.x;
        const canvasOrdinate = canvas.height / 2 + textData.coordinates.y;
        const textAbscissa = -canvasContext.measureText(textData.content).width / 2;
        const textOrdinate = textData.font_size / 2;

        canvasContext.translate(canvasAbscissa, canvasOrdinate);
        canvasContext.rotate(textData.rotation * Math.PI / 180);
        canvasContext.fillText(textData.content, textAbscissa, textOrdinate);
        canvasContext.restore();
    }
}

import { Injectable } from '@angular/core';
import {IndexedDbService} from "../../client-storage/services/indexed-db.service";
import {ReplaySubject} from "rxjs";
import * as LZUTF8 from 'lzutf8';

@Injectable({
    providedIn: 'root'
})

export class ImageService {
    private gettingImages = false;
    constructor(
        private IndexedDbService: IndexedDbService
    ) {}

    public getImageById(imageId) {
        const dataFetched = new ReplaySubject();

        this.IndexedDbService.getRecords('image_db', 'sized_images', 'image_id', imageId)
            .subscribe(
                (sizedImages: any) => {
                    const formattedSizedImages = {};

                    for (let index = 0; index < sizedImages.length; index++) {
                        formattedSizedImages[sizedImages[index].width] =
                            LZUTF8.decompress(sizedImages[index].image, {inputEncoding: 'StorageBinaryString'})
                    }

                    dataFetched.next(formattedSizedImages);
                }
            );

        return dataFetched.asObservable();
    }
}

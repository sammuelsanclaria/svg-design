import { Injectable } from '@angular/core';
import {BehaviorSubject, ReplaySubject} from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class DesignService {
    // public designDataObservable = new BehaviorSubject({});
    // public designDataUpdateObservable = new ReplaySubject();
    //
    // public registerGlobalDesign(designData) {
    //     this.designDataObservable.next(designData);
    //
    //     return this.designDataUpdateObservable.asObservable();
    // }
    //
    // public listenToGlobalDesign() {
    //     return this.designDataObservable.asObservable();
    // }
    //
    // public updateDesignCanvas(designData) {
    //     this.designDataUpdateObservable.next(designData);
    // }
    //
    // public updateDesignElement(designData) {
    //     this.designDataUpdateObservable.next(designData);
    // }

    public fetchDesignData(designId) {
        return {
            page: {
                sections: [
                    {
                        type: 'BACK_COVER',
                        name: 'Back Cover',
                        size: {
                            width: 800,
                            height: 600
                        }
                    }
                ]
            },
            elements: [
                {
                    type: 'IMAGE',
                    size: {
                        width: 300,
                        height: 300
                    },
                    coordinates: {
                        x: 100,
                        y: 100
                    },
                    clipping: {
                        type: 'STAR',
                        coordinates: {
                            x: 0,
                            y: 0
                        },
                        size: {
                            width: 300
                        }
                    },
                    rotation: 0,
                    image_id: 1,
                    filters: {}
                }
            ]
        };
    }
}

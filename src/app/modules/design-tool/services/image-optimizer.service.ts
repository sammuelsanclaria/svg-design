import { Injectable } from '@angular/core';
import {FileReaderService} from "./file-reader.service";
import {ReplaySubject} from "rxjs";
import * as LZUTF8 from 'lzutf8';

@Injectable({
    providedIn: 'root'
})

export class ImageOptimizerService {
    constructor(
        private FileReaderService: FileReaderService
    ) {}

    public resizeImage(file, sizes) {
        const onImageSized = new ReplaySubject();

        this.FileReaderService.fileToImage(file)
            .subscribe(
                (image: any) => {
                    const canvas: any = document.createElement('canvas');
                    const sizedImages = [];

                    for (let index = 0; index < sizes.length; index++) {
                        if (sizes[index] <= image.width) {

                            canvas.width = sizes[index];
                            canvas.height = image.height * (sizes[index] / image.width);
                            canvas.getContext('2d').drawImage(image, 0, 0, canvas.width, canvas.height);

                            sizedImages.push({
                                width: sizes[index],
                                height: canvas.height,
                                image: LZUTF8.compress(canvas.toDataURL('image/jpeg', 0.86),
                                    {outputEncoding: 'StorageBinaryString'})
                            });
                        }
                    }

                    console.log('ORIGINAL BASE 64');
                    console.log(image.src);
                    console.log('COMPRESSED');
                    console.log(LZUTF8.compress(image.src, {outputEncoding: 'StorageBinaryString'}));

                    onImageSized.next({
                        original_image: {
                            image: LZUTF8.compress(image.src, {outputEncoding: 'StorageBinaryString'}),
                            height: image.height,
                            width: image.width
                        },
                        sized_images: sizedImages
                    });
                });

        return onImageSized.asObservable();
    }
}

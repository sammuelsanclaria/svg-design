import { Injectable } from '@angular/core';
import {ReplaySubject} from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class FileReaderService {
    public fileToImage(file) {
        const fileReader = new FileReader();
        const onfileLoad = new ReplaySubject();

        fileReader.onload = () => {
            const image = new Image();

            image.onload = () => {
                onfileLoad.next(image);
            };

            image.src = fileReader.result as string;
        };

        fileReader.readAsDataURL(file);

        return onfileLoad;
    }
}

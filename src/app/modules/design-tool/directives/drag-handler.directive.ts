import {AfterViewInit, Directive, Input, NgModule} from '@angular/core';
import {DragHandlerService} from "../services/drag-handler.service";
import {CoordinateService} from "../services/coordinate.service";

@Directive({
    selector: '[drag-handler]'
})
export class DragHandlerDirective implements AfterViewInit {
    @Input() dragHandlerCode: any;
    @Input() element: any;
    @Input() container: any;
    @Input() dragHandler: any;

    constructor(
        private DragHandlerService: DragHandlerService
    ) {}

    ngAfterViewInit() {
        let callback = {};

        switch (this.dragHandlerCode) {
            case('RIGHT'):
                callback = this.rightDragHandlerDrag;
                break;
        }

        this.DragHandlerService.bindDragEventsToElement(this.container, this.dragHandler);
    }

    private rightDragHandlerDrag(displacement, element, CoordinateService) {
        const elementRotation = element.rotation % 360;
        const hypotenuse = CoordinateService.getSideResizeObject(elementRotation, displacement, 'R');
        const centerCompensatorCoordinates = CoordinateService.getCenterCompensatorCoordinates(hypotenuse, elementRotation, 'R');

        // if (this.panelStatuses.cropping) {
        //     // when cropping is active
        //     this.designData.elements[this.selectedElementIndex].cropping.size.width += this.coordinateService.getCroppingIncrease(
        //         this.designData.elements[this.selectedElementIndex],
        //         hypotenuse
        //     );
        // }

        element.coordinates.x -= centerCompensatorCoordinates.xCompensator;
        element.coordinates.y -= centerCompensatorCoordinates.yCompensator;

        const secondaryHypotenuse = CoordinateService.getSecondaryHypotenuse(
            element,
            hypotenuse,
            'R'
        );

        element.size.width += hypotenuse;

    }
}

@NgModule({
    declarations: [DragHandlerDirective],
    exports: [DragHandlerDirective]
})

export class DragHandlerDirectiveModule { }
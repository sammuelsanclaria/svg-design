import {
    AfterViewInit,
    Directive,
    ElementRef,
    HostListener,
    Input,
    NgModule
} from '@angular/core';
import {DragHandlerService} from "../../services/drag-handler.service";

@Directive({
    selector: '[svg-image-wrap]'
})
export class SvgImageWrapDirective implements AfterViewInit {
    @Input() container: any;
    @Input() element: any;

    @HostListener('click') onClick() {
        this.getTransformation();
    }

    private getTransformation() {
        const transformation = 'translate(' + this.element.coordinates.x
            + ' ' + this.element.coordinates.y + ') rotate(' + this.element.rotation
            + ' ' + (this.element.size.width / 2)
            + ' ' + (this.element.size.height / 2) + ')';

        this.el.nativeElement.setAttributeNS(null, 'width', this.element.size.width);
        this.el.nativeElement.setAttributeNS(null, 'height', this.element.size.height);
        this.el.nativeElement.setAttributeNS(null, 'transform', transformation);
    }

    constructor(
        private DragHandlerService: DragHandlerService,
        private el: ElementRef
    ) {}

    ngAfterViewInit() {
        this.getTransformation();
        this.DragHandlerService.bindDragEventsToElement(this.container, this.el.nativeElement)
            .subscribe(
                (event: any) => {
                    switch (event.event_code) {
                        case 'DRAG_START':
                            break;

                        case 'DRAGGING':
                            break;

                        case 'DRAG_END':
                            break;
                    }
                }
            );
    }
}

@NgModule({
    declarations: [SvgImageWrapDirective],
    exports: [SvgImageWrapDirective]
})

export class SvgImageWrapDirectiveModule { }
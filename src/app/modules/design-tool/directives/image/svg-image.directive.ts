import {
    AfterViewInit,
    Directive,
    ElementRef,
    Input,
    NgModule
} from '@angular/core';
import {DragHandlerService} from "../../services/drag-handler.service";
import {CoordinateService} from "../../services/coordinate.service";

@Directive({
    selector: '[svg-image]'
})
export class SvgImageDirective implements AfterViewInit {
    @Input() container: any;
    @Input() element: any;
    @Input() dragHandlers: any;

    private getTransformation() {
        const transformation = 'translate(' + this.element.coordinates.x
            + ' ' + this.element.coordinates.y + ') rotate(' + this.element.rotation
            + ' ' + (this.element.size.width / 2)
            + ' ' + (this.element.size.height / 2) + ')';

        this.el.nativeElement.setAttributeNS(null, 'width', this.element.size.width);
        this.el.nativeElement.setAttributeNS(null, 'height', this.element.size.height);
        this.el.nativeElement.setAttributeNS(null, 'transform', transformation);

        if (this.dragHandlers.container) {
            this.dragHandlers.container.setAttributeNS(null, 'width', this.element.size.width);
            this.dragHandlers.container.setAttributeNS(null, 'height', this.element.size.height);
            this.dragHandlers.container.setAttributeNS(null, 'transform', transformation);
        }

        if (this.dragHandlers) {
            if (this.dragHandlers.wrap) {
                this.dragHandlers.wrap.setAttributeNS(null, 'width', this.element.size.width);
                this.dragHandlers.wrap.setAttributeNS(null, 'height', this.element.size.height);
            }

            if (this.dragHandlers.bottomLeft) {
                this.dragHandlers.bottomLeft.setAttributeNS(null, 'cy', this.element.size.height);
            }

            if (this.dragHandlers.bottomRight) {
                this.dragHandlers.bottomRight.setAttributeNS(null, 'cx', this.element.size.width);
                this.dragHandlers.bottomRight.setAttributeNS(null, 'cy', this.element.size.height);
            }

            if (this.dragHandlers.rotator) {
                this.dragHandlers.rotator.setAttributeNS(null, 'cx', this.element.size.width / 2);
                this.dragHandlers.rotator.setAttributeNS(null, 'cy', -30);
            }

            if (this.dragHandlers.topRight) {
                this.dragHandlers.topRight.setAttributeNS(null, 'cx', this.element.size.width);
            }
        }
    }

    constructor(
        private CoordinateService: CoordinateService,
        private DragHandlerService: DragHandlerService,
        private el: ElementRef
    ) {}

    ngAfterViewInit() {
        this.getTransformation();
        this.addDragHandlerEvents();
    }

    private addDragHandlerEvents() {
        this.DragHandlerService.bindDragEventsToElement(this.container, this.dragHandlers.bottomRight)
            .subscribe(
                (event: any) => {
                    switch (event.event_code) {
                        case 'DRAG_START':
                            break;

                        case 'DRAGGING':
                            this.bottomRightDrag(event.displacement);
                            break;

                        case 'DRAG_END':
                            break;
                    }
                }
            );

        this.DragHandlerService.bindDragEventsToElement(this.container, this.dragHandlers.bottomLeft)
            .subscribe(
                (event: any) => {
                    switch (event.event_code) {
                        case 'DRAG_START':
                            break;

                        case 'DRAGGING':
                            this.bottomLeftDrag(event.displacement);
                            break;

                        case 'DRAG_END':
                            break;
                    }
                }
            );

        this.DragHandlerService.bindDragEventsToElement(this.container, this.dragHandlers.topRight)
            .subscribe(
                (event: any) => {
                    switch (event.event_code) {
                        case 'DRAG_START':
                            break;

                        case 'DRAGGING':
                            this.topRightDrag(event.displacement);
                            break;

                        case 'DRAG_END':
                            break;
                    }
                }
            );

        this.DragHandlerService.bindDragEventsToElement(this.container, this.dragHandlers.topLeft)
            .subscribe(
                (event: any) => {
                    switch (event.event_code) {
                        case 'DRAG_START':
                            break;

                        case 'DRAGGING':
                            this.topLeftDrag(event.displacement);
                            break;

                        case 'DRAG_END':
                            break;
                    }
                }
            );

        this.DragHandlerService.bindDragEventsToElement(this.container, this.el.nativeElement)
            .subscribe(
                (event: any) => {
                    switch (event.event_code) {
                        case 'DRAG_START':
                            break;

                        case 'DRAGGING':
                            this.imageDrag(event.displacement);
                            break;

                        case 'DRAG_END':
                            break;
                    }
                }
            );

        this.DragHandlerService.bindDragEventsToElement(this.container, this.dragHandlers.rotator)
            .subscribe(
                (event: any) => {
                    switch (event.event_code) {
                        case 'DRAG_START':
                            break;

                        case 'DRAGGING':
                            this.rotateImage(event.displacement);
                            break;

                        case 'DRAG_END':
                            break;
                    }
                }
            );
    }

    private rotateImage(displacement) {
        const newRotation = this.CoordinateService.getCursorAngleFromElementOrigin(this.element, displacement);
        this.element.rotation = newRotation % 360;

        this.getTransformation();
    }

    private bottomLeftDrag(displacement) {
        const elementRotation = this.element.rotation % 360;
        const cursorAngle = Math.abs((this.CoordinateService.getCursorAngleFromElementOrigin(this.element, displacement) - ((elementRotation + 180) % 360)));
        const cornerAngle = Math.abs((this.CoordinateService.getCornerAngle(this.element)) % 360);

        if (cursorAngle > cornerAngle) {
            this.increaseRightWidth(displacement, 'L', 'B', 'BL');
        } else {
            this.increaseBottomHeight(displacement, 'B', 'L', 'BL');
        }

        this.getTransformation();
    }

    private bottomRightDrag(displacement) {
        const elementRotation = this.element.rotation % 360;
        const cursorAngle = Math.abs((this.CoordinateService.getCursorAngleFromElementOrigin(this.element, displacement) - ((elementRotation + 90) % 360)));
        const cornerAngle = Math.abs((this.CoordinateService.getCornerAngle(this.element)) % 360);

        if (cursorAngle > cornerAngle) {
            this.increaseBottomHeight(displacement, 'B', 'R', 'BR');
        } else {
            this.increaseRightWidth(displacement, 'R', 'B', 'BR');
        }

        this.getTransformation();
    }

    private topRightDrag(displacement) {
        const elementRotation = this.element.rotation % 360;
        const cursorAngle = Math.abs((this.CoordinateService.getCursorAngleFromElementOrigin(this.element, displacement) - ((elementRotation) % 360)));
        const cornerAngle = Math.abs((this.CoordinateService.getCornerAngle(this.element)) % 360);

        if (cursorAngle < cornerAngle) {
            this.increaseBottomHeight(displacement, 'T', 'R', 'TR');
        } else {
            this.increaseRightWidth(displacement, 'R', 'T', 'TR');
        }

        this.getTransformation();
    }

    private topLeftDrag(displacement) {
        const elementRotation = this.element.rotation % 360;
        const cursorAngle = Math.abs((this.CoordinateService.getCursorAngleFromElementOrigin(this.element, displacement) - ((elementRotation + 270) % 360)));
        const cornerAngle = Math.abs((this.CoordinateService.getCornerAngle(this.element)) % 360);

        if (cursorAngle > cornerAngle) {
            this.increaseBottomHeight(displacement, 'T', 'L', 'TL');
        } else {
            this.increaseRightWidth(displacement, 'L', 'T', 'TL');
        }

        this.getTransformation();
    }

    private increaseRightWidth(displacement, primarySideCode, secondarySideCode, cornerDragHandlerCode) {
        const elementRotation = this.element.rotation % 360;
        const hypotenuse = this.CoordinateService.getSideResizeObject(elementRotation, displacement, primarySideCode);
        const centerCompensatorCoordinates = this.CoordinateService.getCenterCompensatorCoordinates(hypotenuse, elementRotation, primarySideCode);

        this.element.coordinates.x -= centerCompensatorCoordinates.xCompensator;
        this.element.coordinates.y -= centerCompensatorCoordinates.yCompensator;
        this.element.size.width += hypotenuse;

        const secondaryHypotenuse = this.CoordinateService.getSecondaryHypotenuse(
            this.element,
            hypotenuse,
            primarySideCode
        );

        // only when not cropping
        const secondaryCenterCompensatorCoordinates = this.CoordinateService.getCenterCompensatorCoordinates(
            secondaryHypotenuse,
            elementRotation,
            secondarySideCode
        );

        this.element.coordinates.y -= secondaryCenterCompensatorCoordinates.xCompensator;
        this.element.coordinates.x += secondaryCenterCompensatorCoordinates.yCompensator;
        this.element.size.height += secondaryHypotenuse;

        return {
            height_increase: secondaryHypotenuse,
            width_increase: hypotenuse
        }
    }

    private increaseBottomHeight(displacement, primarySideCode, secondarySideCode, cornerDragHandlerCode) {
        const elementRotation = this.element.rotation % 360;
        const hypotenuse = this.CoordinateService.getSideResizeObject(elementRotation, displacement, primarySideCode);
        const centerCompensatorCoordinates = this.CoordinateService.getCenterCompensatorCoordinates(hypotenuse, elementRotation, primarySideCode);

        this.element.coordinates.y -= centerCompensatorCoordinates.xCompensator;
        this.element.coordinates.x += centerCompensatorCoordinates.yCompensator;
        this.element.size.height += hypotenuse;

        const secondaryHypotenuse = this.CoordinateService.getSecondaryHypotenuse(
            this.element,
            hypotenuse,
            primarySideCode
        );

        // only when not cropping
        const secondaryCenterCompensatorCoordinates = this.CoordinateService.getCenterCompensatorCoordinates(
            secondaryHypotenuse,
            elementRotation,
            secondarySideCode
        );

        this.element.coordinates.x -= secondaryCenterCompensatorCoordinates.xCompensator;
        this.element.coordinates.y -= secondaryCenterCompensatorCoordinates.yCompensator;
        this.element.size.width += secondaryHypotenuse;

        return {
            width_increase: secondaryHypotenuse,
            height_increase: hypotenuse
        }
    }

    private imageDrag(displacement) {
        this.element.coordinates.x += displacement.xDisplacement;
        this.element.coordinates.y += displacement.yDisplacement;
        this.getTransformation();
    }
}

@NgModule({
    declarations: [SvgImageDirective],
    exports: [SvgImageDirective]
})

export class ImageDirectiveModule { }
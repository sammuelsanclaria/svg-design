import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ImageService} from '../../services/image.service';

@Component({
    selector: 'workspace',
    templateUrl: './workspace.component.html'
})

export class WorkspaceComponent implements OnInit {
    @ViewChild('workspace', {static: false}) workspace: ElementRef;

    public designData = null;
    public sizedImages: any = {};
    public fittedImages: any = {};
    public workSpaceDimensions: any = {
        width: 0,
        height: 0
    };

    constructor(
        public CanvasImageService: CanvasImageService,
        public DragHandlerService: DragHandlerService,
        private CoordinateService: CoordinateService,
        private ImageService: ImageService,
        private DomSanitizer: DomSanitizer
    ) {}

    ngOnInit(): void {
    }

    public updateDesignData(newDesignData) {
        for (let index = 0; index < newDesignData.elements.length; index++) {
            const element = newDesignData.elements[index];
            if (element.type === 'IMAGE') {
                if (!this.sizedImages[element.image_id]) {
                    this.ImageService.getImageById(element.image_id)
                        .subscribe(
                            sizedImages => {
                                this.sizedImages[element.image_id] = sizedImages;
                                this.optimizeRenderedImages(newDesignData);
                            }
                        );
                } else {
                    this.optimizeRenderedImages(newDesignData);
                }
            }
        }

        this.designData = newDesignData;
        this.setWorkspaceDimension();
    }

    public optimizeRenderedImages(newDesignData) {
        for (let index = 0; index < newDesignData.elements.length; index++) {
            const element = newDesignData.elements[index];
            if (element.type === 'IMAGE' && this.sizedImages[element.image_id]) {
                this.fittedImages[element.image_id] =
                    this.CanvasImageService.getAppropriateSizedImage(
                        element.size.width,
                        this.sizedImages[element.image_id]
                    );
            }
        }
    }

    private setWorkspaceDimension() {
        const dimensions = {width: 0,height: 0};

        for (let index = 0; index < this.designData.page.sections.length; index++) {
            dimensions.width += this.designData.page.sections[index].size.width;

            if (this.designData.page.sections[index].size.height > dimensions.height) {
                dimensions.height = this.designData.page.sections[index].size.height;
            }
        }

        this.workSpaceDimensions = dimensions;
    }

    public getCursorClass(dragHandlerAngle, element) {
        let handlerAngle = (element.rotation + dragHandlerAngle) % 360;
        handlerAngle = handlerAngle >= 360 ? handlerAngle - 360 : handlerAngle;

        if (handlerAngle > 337.5 || handlerAngle <= 22.5) {
            return this.DomSanitizer.bypassSecurityTrustStyle('cursor: w-resize');

        } else if (handlerAngle > 22.5 && handlerAngle <= 67.5) {
            return this.DomSanitizer.bypassSecurityTrustStyle('cursor: se-resize');

        } else if (handlerAngle > 67.5 && handlerAngle <= 112.5) {
            return this.DomSanitizer.bypassSecurityTrustStyle('cursor: s-resize');

        } else if (handlerAngle > 112.5 && handlerAngle <= 157.5) {
            return this.DomSanitizer.bypassSecurityTrustStyle('cursor: sw-resize');

        } else if (handlerAngle > 157.5 && handlerAngle <= 202.5) {
            return this.DomSanitizer.bypassSecurityTrustStyle('cursor: w-resize');

        } else if (handlerAngle > 202.5 && handlerAngle <= 247.5) {
            return this.DomSanitizer.bypassSecurityTrustStyle('cursor: nw-resize');

        } else if (handlerAngle > 247.5 && handlerAngle <= 292.5) {
            return this.DomSanitizer.bypassSecurityTrustStyle('cursor: n-resize');

        } else if (handlerAngle > 292.5 && handlerAngle <= 337.5) {
            return this.DomSanitizer.bypassSecurityTrustStyle('cursor: ne-resize');
        }
    }

    public getElementRotation(element) {
        return this.DomSanitizer.bypassSecurityTrustStyle('transform: translate(100, 100) rotate(45 60 60)');
        return this.DomSanitizer.bypassSecurityTrustStyle('transform:rotate(' + element.rotation + ' ' + (element.size.width / 2) + ' ' + (element.size.height / 2) + ')');
    }
}

import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {CanvasImageService} from "../../services/canvas-image.service";
import {DragHandlerService} from "../../services/drag-handler.service";
import {DomSanitizer} from "@angular/platform-browser";
import {CoordinateService} from "../../services/coordinate.service";
import {ImageDirectiveModule} from "../../directives/image/svg-image.directive";
import {DragHandlerDirectiveModule} from "../../directives/drag-handler.directive";

@NgModule({
    imports: [CommonModule, ImageDirectiveModule, DragHandlerDirectiveModule],
    declarations: [WorkspaceComponent],
    exports: [WorkspaceComponent]
})

export class WorkspaceModule {}

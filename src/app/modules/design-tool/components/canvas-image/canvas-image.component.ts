import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'canvas-image',
    templateUrl: './canvas-image.component.html'
})

export class CanvasImageComponent implements OnInit {
    @Input() imageData: any = {};

    constructor() {}
    ngOnInit(): void {}

    public getAppropriateImage() {
    }
}

import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule],
    declarations: [CanvasImageComponent],
    exports: [CanvasImageComponent]
})

export class CanvasImageModule {}

import { Injectable } from '@angular/core';
import idb from 'idb';
import {ReplaySubject} from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class IndexedDbService
{
    constructor() {}
    private databases: any = {};

    public upgradeDatabase(version = 0) {
        if (window.indexedDB) {
            this.databases['image_db'] = idb.open('images-db', version, (upgradeDb) => {
                switch (upgradeDb.oldVersion) {
                    case 0:
                        // images table
                        upgradeDb.createObjectStore('images', {keyPath: 'image_id', autoIncrement : true});
                        const imagesOS = upgradeDb.transaction.objectStore('images');
                        imagesOS.createIndex('width', 'width', {unique : false});
                        imagesOS.createIndex('height', 'height', {unique : false});
                        imagesOS.createIndex('image', 'image', {unique : false});
                        imagesOS.createIndex('uploaded_on', 'uploaded_on', {unique : false});

                        // sized images table
                        upgradeDb.createObjectStore('sized_images', {keyPath: 'sized_image_id', autoIncrement : true});
                        const sizedImagesOS = upgradeDb.transaction.objectStore('sized_images');
                        sizedImagesOS.createIndex('image_id', 'image_id', {unique : false});
                        sizedImagesOS.createIndex('width', 'width', {unique : false});
                        sizedImagesOS.createIndex('height', 'height', {unique : false});
                        sizedImagesOS.createIndex('image', 'image', {unique : false});
                }
            });

            return true;
        }

        return false;
    }

    public addIndexDBRecord(database, storeName, data) {
        const inserted = new ReplaySubject();
        let transaction = {};

        this.databases[database].then(db => {
            const tx = db.transaction(storeName, 'readwrite');
            transaction = tx.objectStore(storeName).add(data);
            return tx.complete;

        }).then(() => {inserted.next(transaction['request'].result)});

        return inserted.asObservable();
    }

    public getRecords(database, storeName, field, value) {
        const dataFetched = new ReplaySubject();
        let transaction = {};

        this.databases[database].then(db => {
            const tx = db.transaction(storeName, 'readwrite');
            transaction = tx.objectStore(storeName).index(field).getAll(value);

            return tx.complete;

        }).then(() => {
            dataFetched.next(transaction['request'].result)
        });

        return dataFetched.asObservable();
    }

    public updateIndexDBRecord(database, storeName, selector, data) {
        this.databases[database].then(db => {
            const tx = db.transaction(storeName, 'readwrite');
            tx.objectStore(storeName).put(selector, data);
            return tx.complete;
        }).then(() => console.log('success'));
    }
}

import {Component, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-root',
    template: '<router-outlet></router-outlet>',
    styleUrls: ['./global.less'],
    encapsulation: ViewEncapsulation.None
})

export class AppComponent {}

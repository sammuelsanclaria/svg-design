import {NgModule} from '@angular/core';
import {ToolComponent} from './tool.component';
import {ToolRoutingModule} from './tool-routing.module';
import {MatButtonModule, MatSidenavModule} from '@angular/material';
import {CommonModule} from '@angular/common';
import {CanvasModule} from '../../modules/design-tool/components/canvas/workspace-canvas.component';

@NgModule({
    imports: [
        // module imports
        CommonModule,
        MatSidenavModule,
        MatButtonModule,

        // routing modules
        ToolRoutingModule,

        // component modules
        CanvasModule

        // service modules
    ],
    declarations: [
        ToolComponent
    ]
})

export class ToolModule {}

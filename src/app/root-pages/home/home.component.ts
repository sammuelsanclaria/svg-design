import {AfterViewInit, Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatBottomSheet, MatSnackBar} from '@angular/material';
import {BottomSheetComponent} from '../../modules/layout/components/buttom-sheet/bottom-sheet.component';
import {ImageOptimizerService} from "../../modules/design-tool/services/image-optimizer.service";
import {IndexedDbService} from "../../modules/client-storage/services/indexed-db.service";
import {DesignService} from "../../modules/design-tool/services/design.service";
import {WorkspaceComponent} from "../../modules/design-tool/components/workspace/workspace.component";

@Component({
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less'],
    encapsulation: ViewEncapsulation.None
})

export class HomeComponent implements OnInit, AfterViewInit {
    @ViewChild('uploader', {static: false}) uploader: ElementRef;
    @ViewChild('workspace', {static: false}) workspace: WorkspaceComponent;

    public floatingButtons = [
        {
            icon: 'fa fa-upload',
            color: 'accent',
            events: {
                click: (event) => {
                    this.openUplaoderBottomSheet();
                }
            }
        },
        {
            icon: 'fa fa-search-plus',
            events: {
                click: (event) => {
                }
            }
        },
        {
            icon: 'fa fa-search-minus',
            color: 'warn',
            events: {
                click: (event) => {
                }
            }
        }
    ];

    public uploaderOptions = [
        {
            label: 'File Explorer',
            sub_label: 'Upload images from local drive',
            events: {
                click: (event) => {
                    this.uploader.nativeElement.click();
                }
            }
        },
        {
            label: 'Google Drive',
            sub_label: 'Select image from your Google Drive',
            events: {
                click: (event) => {
                    console.log('Opening Google Drive');
                }
            }
        },
        {
            label: 'One Drive',
            sub_label: 'Select image from your One Drive',
            events: {
                click: (event) => {
                    console.log('Opening One Drive');
                }
            }
        }
    ];

    public sizes = [1200, 750, 450, 250, 150];
    private indexDb = null;
    private designData = null;

    constructor(
        private BottomSheet: MatBottomSheet,
        private SnackBar: MatSnackBar,
        private ImageOptimizerService: ImageOptimizerService,
        private IndexedDbService: IndexedDbService,
        private DesignService: DesignService,
    ) {}

    ngOnInit(): void {
        this.indexDb = this.IndexedDbService.upgradeDatabase(1);
        this.designData = this.DesignService.fetchDesignData(1);
    }

    ngAfterViewInit(): void {
        this.workspace.updateDesignData(this.designData);
    }

    public openUplaoderBottomSheet() {
        this.BottomSheet.open(BottomSheetComponent, {
            data: {
                options: this.uploaderOptions
            }
        });
    }

    public generateSizedImages(event) {
        this.ImageOptimizerService.resizeImage(this.uploader.nativeElement.files[0], this.sizes)
            .subscribe(
                (image: any) => {
                    if (image.original_image) {
                        image.original_image.uploaded_on = Date.now();

                        this.IndexedDbService.addIndexDBRecord('image_db', 'images', image.original_image)
                            .subscribe(
                                imageId => {
                                    for (let index = 0; index < image.sized_images.length; index++) {
                                        image.sized_images[index].image_id = imageId;
                                        this.IndexedDbService.addIndexDBRecord('image_db', 'sized_images', image.sized_images[index])
                                            .subscribe(
                                                sizedImageId => {
                                                    if (index === image.sized_images.length - 1) {
                                                        this.addImage(imageId, image);
                                                        this.uploader.nativeElement.value = '';
                                                        this.SnackBar.open('Image successfully loaded!', 'Dismiss', {
                                                            duration: 3000
                                                        });
                                                    }
                                                });
                                    }
                                }
                            );

                    } else {
                        this.SnackBar.open('Failed to load image.', 'Dismiss', {
                            duration: 3000
                        });
                    }
                }
            );
    }

    private addImage(imageId, image) {
        this.designData.elements.push({
            type: 'IMAGE',
            size: {
                width: image.original_image.width,
                height: image.original_image.height
            },
            clipping: {
                type: 'CIRCLE',
                coordinates: {
                    x: 0,
                    y: 0
                },
                size: {
                    width: image.original_image.width
                }
            },
            rotation: 30,
            coordinates: {
                x: 0,
                y: 0
            },
            image_id: imageId,
            filters: {}
        });

        this.workspace.updateDesignData(this.designData);
    }
}

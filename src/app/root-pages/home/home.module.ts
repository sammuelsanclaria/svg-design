import {NgModule} from '@angular/core';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home-routing.module';
import {WorkspaceModule} from '../../modules/design-tool/components/workspace/workspace.component';
import {FloatingPanelModule} from '../../modules/layout/components/floating-panel/floating-panel.component';
import {MatButtonModule, MatSnackBarModule} from '@angular/material';
import {BottomSheetModule} from "../../modules/layout/components/buttom-sheet/bottom-sheet.component";

@NgModule({
    imports: [
        MatButtonModule,
        BottomSheetModule,
        MatSnackBarModule,

        // routing modules
        HomeRoutingModule,

        // component modules
        WorkspaceModule,
        FloatingPanelModule

        // service modules
    ],
    declarations: [
        HomeComponent
    ]
})

export class HomeModule {}

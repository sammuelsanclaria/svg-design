import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: './root-pages/home/home.module#HomeModule',
    },
    {
        path: 'design',
        loadChildren: './root-pages/tool/tool.module#ToolModule',
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes,  {onSameUrlNavigation: 'reload'})],
    exports: [RouterModule]
})
export class AppRoutingModule {}
